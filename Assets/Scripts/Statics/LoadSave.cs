﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
public static class LoadSave {

	public static bool hasSave {
		get {
			return PlayerPrefs.HasKey(SAVE_FILE);
		}
	}

	static Main mainRef;
	const string SAVE_FILE = "saveFile";

	public static void SetMain(Main _mainRef) {
		mainRef = _mainRef;
	}

	public static void SaveGame() {
		Debug.Log("saveGame");
		JSONClass tmpSave = new JSONClass();
		tmpSave.Add("gameTime",mainRef.gameTimer.ToString());
		tmpSave.Add("target",mainRef.target.ToJson());
		List<Enemy> tmpEnemyList = Enemy.enemies;
		JSONArray tmpEnemies = new JSONArray();
		for (int i = 0; i < tmpEnemyList.Count; i++) {
			tmpEnemies.Add(tmpEnemyList[i].ToJson());
		}
		tmpSave.Add("enemies",tmpEnemies);
		tmpSave.Add("replay",Replay.ToJson());
		PlayerPrefs.SetString(SAVE_FILE,tmpSave.SaveToBase64());
	}
	public static void LoadGame() {
		JSONClass tmpSave = JSONNode.LoadFromBase64(PlayerPrefs.GetString(SAVE_FILE)) as JSONClass;
		mainRef.target.FromJson(tmpSave["target"] as JSONClass);
		mainRef.gameTimer = tmpSave["gameTime"].AsFloat;
		JSONArray tmpEnemies = tmpSave["enemies"] as JSONArray;
		for (int i = 0; i < tmpEnemies.Count; i++) {
			Enemy.MakeEnemy(tmpEnemies[i] as JSONClass,mainRef);
		}
		Replay.FromJson(tmpSave["replay"] as JSONClass);

	}
	public static void Clear() {
		PlayerPrefs.DeleteKey(SAVE_FILE);
	}
}
