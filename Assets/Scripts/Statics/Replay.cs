﻿using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public static class Replay {

	public static Vector3 targetPos;

	static List<EnemySpawnEvent> enemySpawnHistory;
	static Main mainRef;

	public static void SetMain(Main _mainRef) {
		mainRef = _mainRef;
		enemySpawnHistory = new List<EnemySpawnEvent>();
	}

	public static void MarkTarget(Vector3 _targetPos) {
		targetPos = _targetPos;
	}
	public static void EnemySpawn(Vector3 _spawnPos, float _spawnTime) {
		EnemySpawnEvent tmpESE = new EnemySpawnEvent();
		tmpESE.spawnPos = _spawnPos;
		tmpESE.spawnTime = _spawnTime;
		enemySpawnHistory.Add(tmpESE);
	}
	public static void CheckEvent(float _currentTime) {
		if (enemySpawnHistory.Count == 0) return;
		while (enemySpawnHistory[0].spawnTime <= _currentTime) {
			Debug.Log(enemySpawnHistory[0].spawnTime);
			Enemy.MakeEnemy(enemySpawnHistory[0].spawnPos,mainRef);
			enemySpawnHistory.RemoveAt(0);
			if (enemySpawnHistory.Count == 0) {
				break;
			}
		}
	}
	public static void Clear() {
		targetPos = Vector3.zero;
		enemySpawnHistory.Clear();
	}

	public static void FromJson(JSONClass _json) {
		Clear();
		targetPos = (_json["targetPos"] as JSONArray).AsVector3;
		JSONArray tmpEnemies = _json["enemies"] as JSONArray;
		for (int i = 0; i < tmpEnemies.Count; i++) {
			JSONClass tmpEnemy = tmpEnemies[i] as JSONClass;
			EnemySpawnEvent tmpESE = new EnemySpawnEvent();
			tmpESE.spawnTime = tmpEnemy["spawnTime"].AsFloat;
			tmpESE.spawnPos = (tmpEnemy["spawnPos"] as JSONArray).AsVector3;
			enemySpawnHistory.Add(tmpESE);
		}
	}
	public static JSONClass ToJson() {
		JSONClass tmpSave = new JSONClass();
		tmpSave.Add("targetPos",targetPos.ToJson());
		JSONArray tmpEnemies = new JSONArray();
		for (int i = 0; i < enemySpawnHistory.Count; i++) {
			JSONClass tmpEnemy = new JSONClass();
			tmpEnemy.Add("spawnTime",enemySpawnHistory[i].spawnTime.ToString());
			tmpEnemy.Add("spawnPos",enemySpawnHistory[i].spawnPos.ToJson());
			tmpEnemies.Add(tmpEnemy);
		}
		tmpSave.Add("enemies",tmpEnemies);
		return tmpSave;
	}
}

class EnemySpawnEvent {
	public float spawnTime;
	public Vector3 spawnPos;
}