﻿using UnityEngine;
using UnityEngine.UI;

public class UiHandler : MonoBehaviour {

	public Text hpText;
	public Text currentHp;

	public GameObject pauseMenu;
	public GameObject gameOverMenu;
	public GameObject replayButton;
	public GameObject choiceMenu;
	public Text enemyText;

	public void OnGamePause(bool _isPause) {
		pauseMenu.SetActive(_isPause);
	}
	public void OnGameEnd(int _enemyCount,bool _isReplay) {
		gameOverMenu.SetActive(true);
		enemyText.text = _enemyCount.ToString();
		replayButton.SetActive(!_isReplay);
	}
	public void ShowChoiceMenu() {
		choiceMenu.SetActive(true);
	}
	public void HideMenus() {
		choiceMenu.SetActive(false);
		gameOverMenu.SetActive(false);
		pauseMenu.SetActive(false);
	}
}
