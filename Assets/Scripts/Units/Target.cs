﻿using UnityEngine;
using SimpleJSON;

public class Target : Unit {
	public JSONClass ToJson() {
		JSONClass tmpJson = new JSONClass();
		tmpJson.Add("position",transform.position.ToJson());
		tmpJson.Add("attackCounter",attackRateCounter.ToString());
		tmpJson.Add("health",Health.ToString());
		return tmpJson;

	}
	public void FromJson(JSONClass _json) {
		transform.position = (_json["position"] as JSONArray).AsVector3;
		attackRateCounter = _json["attackCounter"].AsFloat;
		Health = _json["health"].AsInt;
	}
	protected override void Update() {
		base.Update();
		if (CanFire()) {
			for (int i = Enemy.enemies.Count-1; i >= 0; i--) {
				if (CanShoot(Enemy.enemies[i])) {
					AttackUnit(Enemy.enemies[i]);
					break;
				}
			}
		}
	}
	protected override void OnDeath () {
		base.OnDeath ();
		parent.OnTargetDeath();
	}
}
