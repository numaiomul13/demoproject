﻿using UnityEngine;

public abstract class Unit : MonoBehaviour {

	public LineRenderer attackLine;

	protected Main parent;
	protected float attackRateCounter;

	[SerializeField]
	private Color attackColor;
	[SerializeField]
	private int attackDamage;
	[SerializeField]
	private float attackRange;
	[SerializeField]
	private float attackRate;
	[SerializeField]
	private int health;
	public int Health {
		get {
			return health;
		}
		protected set {
			health = value;
			CheckState();
		}
	}

	private const float attackLineDuration = 0.2f;
	private float attackLineCounter;

	public virtual void Awake() {
		attackLine.SetColors(attackColor,attackColor);
		attackLine.SetPosition(0,transform.position);
		attackLine.SetPosition(1,transform.position);
	}

	public void RemoveHealth(int _attackDamage){
		Health -= _attackDamage;
	}
	public virtual void Init(Main _parent) {
		parent = _parent;
	}
	public virtual void OnGamePause(bool _isPause) {
		enabled = !_isPause;
	}

	protected void CheckState() {
		if (health <= 0) {
			OnDeath();
		}
	}
	protected bool CanFire() {
		return attackRateCounter >= attackRate;
	}
	protected bool CanShoot(Unit _target) {
		return Vector3.Distance(transform.position,_target.transform.position) <= attackRange;
	}
	protected virtual void Update() {
		attackRateCounter += Time.deltaTime;
		attackLineCounter += Time.deltaTime;
		if (attackLine.enabled && attackLineDuration <= attackLineDuration) {
			attackLine.enabled = false;
		}
	}
	protected virtual void OnDeath() {
		Destroy(gameObject);
	}
	protected virtual void AttackUnit(Unit _target) {
		_target.RemoveHealth(attackDamage);
		attackRateCounter = 0f;
		attackLine.enabled = true;
		attackLine.SetPosition(0,transform.position);
		attackLine.SetPosition(1,_target.transform.position);
		attackLineCounter = 0f;
	}
}
