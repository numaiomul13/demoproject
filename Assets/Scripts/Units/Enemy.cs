﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SimpleJSON;
using System.Collections.Generic;
public class Enemy : Unit {

	[HideInInspector]
	public static List<Enemy> enemies = new List<Enemy>();

	public const float Y_POS = 0.96f;
	private Target target;
	private NavMeshAgent currentAgent;

	public override void Awake() {
		base.Awake();
		currentAgent = GetComponent<NavMeshAgent>();
	}

	public static void MakeEnemy(JSONClass _json,Main _mainRef) {
		Enemy tmpEnemy = GameObject.Instantiate(_mainRef.enemyPrefab) as Enemy;
		tmpEnemy.FromJson(_json);
		tmpEnemy.Init(_mainRef);
	}
	public static void MakeEnemy(Vector3 _pos, Main _mainRef) {
		Enemy tmpEnemy = Instantiate(_mainRef.enemyPrefab) as Enemy;
		tmpEnemy.transform.position = _pos;
		tmpEnemy.Init(_mainRef);
	}

	public override void Init(Main _parent) {
		base.Init(_parent);
		target = _parent.target;
		currentAgent.destination = target.transform.position;
		enemies.Add(this);
	}
	public override void OnGamePause(bool _isPause) {
		base.OnGamePause(_isPause);
		if (_isPause) {
			currentAgent.Stop();
		} else {
			currentAgent.Resume();
		}
	}
	public void OnGameRestart() {
		Destroy(gameObject);
	}
	public JSONClass ToJson() {
		JSONClass tmpJson = new JSONClass();
		tmpJson.Add("position",transform.position.ToJson());
		tmpJson.Add("rotation",transform.rotation.eulerAngles.ToJson());
		tmpJson.Add("attackCounter",attackRateCounter.ToString());
		tmpJson.Add("health",Health.ToString());
		return tmpJson;

	}
	public void FromJson(JSONClass _json) {
		transform.position = (_json["position"] as JSONArray).AsVector3;
		transform.rotation = Quaternion.Euler((_json["rotation"] as JSONArray).AsVector3);
		attackRateCounter = _json["attackCounter"].AsFloat;
		Health = _json["health"].AsInt;
	}

	public static void ClearList() {
		for (int i = Enemy.enemies.Count-1; i >= 0; i--) {
			Enemy.enemies[i].OnGameRestart();
			Enemy.enemies.RemoveAt(i);
		}
	}

	protected override void Update() {
		base.Update();
		if (CanFire() &&  CanShoot(target)) {
			AttackUnit(target);
		}
	}
	protected override void OnDeath () {
		base.OnDeath ();
		enemies.Remove(this);
		parent.OnEnemyDeath(this);
	}
}