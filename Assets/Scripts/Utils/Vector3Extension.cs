﻿using SimpleJSON;
using UnityEngine;

public static class Vector3Extension {

	public static JSONArray ToJson(this Vector3 _vector) {
		JSONArray tmpJson = new JSONArray();
		tmpJson.Add(_vector.x.ToString());
		tmpJson.Add(_vector.y.ToString());
		tmpJson.Add(_vector.z.ToString());
		return tmpJson;
	}
}
