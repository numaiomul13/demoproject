﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using GameFlags;

public class Main : MonoBehaviour {
	public Enemy enemyPrefab;
	public Target targetPrefab;
	public UiHandler uiRef;
	public GameState gameState;
	public float gameTimer;

	[HideInInspector]
	public Target target;

	private bool isPause;
	private int enemiesDestroyed;
	private float autoSaveCounter = 5f;
	const float autoSaveRate = 5f;

	void Awake() {
		isPause = false;
		LoadSave.SetMain(this);
		Replay.SetMain(this);
	}
	void Start() {
		if (!LoadSave.hasSave) {
			gameState = GameState.GAME;
			StartNewGame();
		} else {
			gameState = GameState.INTRO;
			uiRef.ShowChoiceMenu();
		}
	}
	void Update() {
		if (gameState == GameState.GAME) {
			autoSaveCounter -= Time.deltaTime;
			if (autoSaveCounter <= 0f) {
				LoadSave.SaveGame();
				autoSaveCounter = autoSaveRate;
			}
			if (!isPause) {
				gameTimer += Time.deltaTime;
				if (Input.GetMouseButtonDown(0)) {
					Ray tmpRay = new Ray(Camera.main.ScreenToWorldPoint(Input.mousePosition),Camera.main.transform.forward);
					RaycastHit tmpHit;
					if (Physics.Raycast(tmpRay,out tmpHit)) {
						Vector3 tmpEnemyPos = new Vector3 (tmpHit.point.x,Enemy.Y_POS,tmpHit.point.z);
						Enemy.MakeEnemy(tmpEnemyPos,this);
						Replay.EnemySpawn(tmpEnemyPos,gameTimer);
					}
				}
			}
		} else if (gameState == GameState.REPLAY) {
			gameTimer += Time.deltaTime;
			Replay.CheckEvent(gameTimer);
		}
		if(Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Space)) {
			OnPauseToggle();
		}
		if (gameState != GameState.INTRO) {
			uiRef.currentHp.text = target.Health.ToString(); //this should be triggered by an event released by the target on hit but it's way too late in the night :)
		}
	}

	public void OnPauseToggle() {
		isPause = !isPause;
		target.OnGamePause(isPause);
		for (int i = 0; i < Enemy.enemies.Count; i++) {
			Enemy.enemies[i].OnGamePause(isPause);
		}
		uiRef.OnGamePause(isPause);
	}
	public void OnTargetDeath() {
		enabled = false;
		for (int i = 0; i < Enemy.enemies.Count; i++) {
			Enemy.enemies[i].OnGamePause(true);
		}
		uiRef.OnGameEnd(enemiesDestroyed, gameState == GameState.REPLAY);
		uiRef.currentHp.text = target.Health.ToString();
		Debug.Log("targetDead");
		LoadSave.Clear();
	}
	public void OnEnemyDeath(Enemy _enemy) {
		enemiesDestroyed++;
		LoadSave.SaveGame();
	}
	public void OnGameRestart() {
		LoadSave.Clear();
		Replay.Clear();
		Enemy.ClearList();
		StartNewGame();
	}

	public void StartNewGame() {
		InitTarget();
		Replay.MarkTarget(target.transform.position);
		LoadSave.SaveGame();
		gameTimer = 0f;
		enemiesDestroyed = 0;
		this.enabled = true;
		gameState = GameState.GAME;
	}
	public void LoadGame() {
		InitTarget();
		LoadSave.LoadGame();
		gameState = GameState.GAME;
	}
	public void ReplayGame() {
		Enemy.ClearList();
		LoadSave.Clear();
		gameTimer = 0f;
		enemiesDestroyed = 0;
		InitTarget();
		target.transform.position = Replay.targetPos;
		enabled = true;
		gameState = GameState.REPLAY;
	}
	void InitTarget() {
		target = Instantiate(targetPrefab);
		target.transform.position = new Vector3 (Random.Range(-50f,50f),target.transform.position.y, Random.Range(-50f,50f));
		target.Init(this);
		uiRef.currentHp.text = target.Health.ToString();
		uiRef.hpText.text = "/"+target.Health.ToString();
	}
}

namespace GameFlags {
	public enum GameState {
		INTRO,
		GAME,
		REPLAY
	}
}